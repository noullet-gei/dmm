; dft de signal reel pair
alloc 5 600
;
type A 0
size A 50
read ramp2.dat A
inter A
;
;
rshft A E 0 ; copie pour quant dans domaine spatial
quant E 0.0625
novis
visi A 1
visi E 1
plot signal original en haut, quantifie (0.0625) en bas
;

r2c A
expan A		  ; on en fait un signal pair
dft A B
novis
visi A 1
visi B 1
plot signal reel pair et sa DFT (=DCT)
;
;idft B C
;novis
;visi C 3
;plot le retour
;
rshft B C 0
quant B 0.5
idft B D
novis
visi B 1
visi D 1
plot spectre quantifie (0.5) et transformee inverse
;
size C 100
write spectre.dat C
size B 100
write qspectre.dat B

size A 100
size D 100
novis
visi A 1
visi D 1
plot signal original en haut, reconstruit en bas
;

